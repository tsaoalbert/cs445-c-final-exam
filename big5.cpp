#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <unordered_set>
#include <algorithm>
#include <iomanip>

/*

  * Implement the Big-5 functions for deep copy needed for the following class that has dynamic allocated memory.
  * Look for the key words TODO to fill in the missing codes.
  * When done, turn on the unitTest to check your implementation.
  * The exptected output is shown below.

void unitTest1()
A: (should be 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12) 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12 
B: (should be 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12) 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12 
C: (should be 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0) 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12 
void unitTest2()
A: (should be empty)
B: 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12 
A: 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12 
B: (should be empty)

*/

using namespace std;

template<typename T>
class MyArray {
  // overload insertion operator <<
  friend ostream& operator<< (ostream& out, const MyArray& x ) {
    for (size_t i=0; i < x.m_size ; ++i ) { out << x[i] << " " ; } ;
    return out;
  }

private:
  T* m_arr = nullptr ; 
  size_t m_size = 0 ;

public:
  // use the following functions to reduce the code duplication in assignment operator and copy cstr for
  // both regular and move semantics.
  void mySwap ( MyArray& b ) {
    swap (m_arr, b.m_arr);
    swap (m_size, b.m_size);
  }

   // CSTR
   MyArray (  const vector<T>& v): MyArray ( v.size() )  {
     copy(v.begin(), v.end(), m_arr);
   };

   // CSTR
   MyArray (  size_t n ) {
     // TODO: fill in the missing piece of codes
   };

   // Rules of Big 3: DSTR
   ~MyArray (  ) {
      // TODO: fill in the missing piece of codes
   };

   // Rules of Big 3: Copy Cstr
   MyArray (  const MyArray& x )  {
      // TODO: fill in the missing piece of codes
   }

  // Rules of Big 3: assignment operator 
  MyArray& operator= (  const MyArray& x )  {
    // TODO: fill in the missing piece of codes
    return *this;
  }
   // Rules of Big 5: Move Copy Cstr
   MyArray (  MyArray&& x )  noexcept {
      // TODO: fill in the missing piece of codes
    }

  // Rules of Big 5: move assignment operator
  MyArray& operator= (  MyArray&& x )  noexcept {
    // TODO: fill in the missing piece of codes
    return *this;
  }


  // overload operator []
  inline T& operator[] (size_t i ) { return m_arr[i]; }
  inline const T& operator[] (size_t i ) const { return m_arr[i]; }

};

void unitTest1()
{
  cout << __PRETTY_FUNCTION__ << endl;
  vector<int> v = {7, 4,8,8,10,2,9,8,8,4,0,5,12,2,12} ;
  MyArray <int>* A = new MyArray<int> ( v);
  MyArray <int>* B = new MyArray<int> (0);
  MyArray <int>* C = new MyArray<int> (*A);


  *B = *A;
  *A = *A;
  cout << "A: (should be 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12) " << *A << endl;
  cout << "B: (should be empty) " << *B << endl;
  cout << "C: (should be 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12) " << *C << endl;

  delete A;
  delete B;
  delete C;
}
void unitTest2() {
  cout << __PRETTY_FUNCTION__ << endl;
  vector<int> v = {7, 4,8,8,10,2,9,8,8,4,0,5,12,2,12} ;
  MyArray <int> A (v);
  MyArray <int> C ( {1,2} );
  
  MyArray <int> B ( move(A) ); 
  cout << "A: (should be empty)" << A << endl;
  cout << "B: (should be 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12) " << B << endl;

  A = move (B);
  cout << "A: (should be 7 4 8 8 10 2 9 8 8 4 0 5 12 2 12) " << A << endl;
  cout << "B: (should be empty)" << B << endl;

}
int main (int argc, char* argv[]) { 
  // unitTest1(); 
  // unitTest2(); 
  return 0;
}
