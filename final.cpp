#include <iostream>
#include <vector>

using namespace std;

class Base {
public:
  inline virtual void foo () const { 
    cout << "Base " << __func__ << endl;
    print (); // Derived::print or Base::print ???
  }
  inline virtual void print () const { 
    cout << "Base " << __func__ << endl;
   }
   // virtual void bar () const = 0 ; // does not compile
};

class Derived1: public Base  {
public:
  inline void print () const override { 
    cout << "Derived1 " << __func__ << endl;
   };
};

class Derived2: public Base  {
public:
  inline void print () const override { 
    cout << "Derived2 " << __func__ << endl;
   }
};

/*
void test_seg_fault () {
  Derived1* d1 = new Derived1();
  Base* b = d1;

  d1 = dynamic_cast<Derived1 *> (b) ; // ok

  Derived2* d2 = dynamic_cast<Derived2 *> (b) ; 
  d2->print (); //Segmentation fault , why?
}
*/

void test_static_cast () {
  double d = 2.5 ;
  int x = static_cast < int > (d ); 
  d = x; 
}

void test_move_semantics () {
  vector < unique_ptr<int> >  u ;
  vector < unique_ptr<int> >  v ;
  u = move (v); // ok
  // u = v ; // not, ok 
}

void test_virtual () {

  Derived1* d1 = new Derived1(); 
  Base* b1 = new Base (); 
  Base* b2 = d1;  // d1 is upcast to b2,
  cout << "Line A: " ;
  b1->print (); // Line A: Base print
  cout << "Line B: " ;
  b2->print (); // Line B: Derived1 print
  cout << "Line C: " ;
  d1->print (); // Line C: Derived1 print
  cout << "Line D and E: " ;
  b2->foo ();   // Line D: Base foo
                // Line E: Derived1 print
}
/*
without keyword virtual, function print in derived class re-defines print in base class
*/


/*
int main () {
  test_static_cast();
  test_virtual();
  test_seg_fault () ; // seg fault
  return 0;
}
*/
